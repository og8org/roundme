from flask import Flask, render_template, request, redirect, url_for, send_file
from werkzeug.utils import secure_filename
import json
import datetime
import random
import os
from PIL import Image, ImageDraw

UPLOAD_FOLDER = 'uploads/'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route("/roundme/")
def index():
    return render_template('index.html')

@app.route('/roundme/upload/', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            return render_template('index.html')
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            return render_template('index.html')
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            try:
    	        rr = int(request.form.get('size',100))
            except ValueError:
                rr = 100
            return convertme(filename, rr)
    return render_template('index.html')

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def convertme(filename, rr=100):
    filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    im = Image.open(filepath)
    im = add_corners(im, rr)
    savetopath = ''.join(filepath.split('.')[:-1]) + '.png'
    im.save(savetopath)
    return send_file(savetopath, mimetype='image/png', as_attachment=True)

def add_corners(im, rad):
    circle = Image.new('L', (rad * 2, rad * 2), 0)
    draw = ImageDraw.Draw(circle)
    draw.ellipse((0, 0, rad * 2, rad * 2), fill=255)
    alpha = Image.new('L', im.size, 255)
    w, h = im.size
    alpha.paste(circle.crop((0, 0, rad, rad)), (0, 0))
    alpha.paste(circle.crop((0, rad, rad, rad * 2)), (0, h - rad))
    alpha.paste(circle.crop((rad, 0, rad * 2, rad)), (w - rad, 0))
    alpha.paste(circle.crop((rad, rad, rad * 2, rad * 2)), (w - rad, h - rad))
    im.putalpha(alpha)
    return im

if __name__ == "__main__":
    app.run()
